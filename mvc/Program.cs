﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;

namespace mvc
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .UseApplicationInsights()
                .Build();

            //other methods of WebHostBuilder:
            //ConfigureLogging(Action<ILoggerFactory>)
            //ConfigureServices(Action<IServiceCollection>)
            //UseLoggerFactory(ILoggerFactory) - Specify the ILoggerFactory to be used by the web host.
            //GetSetting(key) and UseSetting (key,value) - Add or replace a setting in the configuration.
            //
            //
            // Методы Configure это extensions из static класса WebHostBuilderExtensions    
            // Configure(IWebHostBuilder, Action<IApplicationBuilder>)
            // UseStartup
            //
            //
            host.Run();
        }
    }
}
