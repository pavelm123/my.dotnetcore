﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions;
using Microsoft.ServiceBus.Messaging;

namespace my.webjobexperiments
{
    public class Functions
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(Functions));


        // This function will get triggered/executed when a new message is written 
        // on an Azure Queue called queue.

        [Disable("Disable_TestJob")] // demonstrates how function could be optioned-out in config

        [Timeout("00:00:05")] //demonstrate that each funciton could have time-out
        public static async Task ProcessTopicMessageAsync(
            [ServiceBusTrigger("test-topic", "test-topic-subscription")] /*[QueueTrigger("queue")]*/ BrokeredMessage message,
            CancellationToken cancellationToken,
            TextWriter log)
        {
            var telemetry = new TelemetryClient();

            try
            {
                Console.WriteLine("Console.WriteLine: " + message);
                Console.Out.WriteLine("Console.Out: " + message);
                Console.Error.WriteLine("Console.Error: " + message);
                log.WriteLine("TextWriterLog.WriteLine: " + message);
                
                // trace output контролируется настройками в Azure: application logging - file system/table storage/blob storage
                // записывается автоматом: timestamp, eventtickcount, instance id, pid, tid(? thread id что ли?)
                System.Diagnostics.Trace.TraceInformation("System.Diagnostics.Trace.TraceInformation: " + message);
                System.Diagnostics.Trace.TraceWarning("TraceWarning WebSite");


                int imax = 4;
                for (int i = 0; i < imax; i++)
                {
                    Thread.Sleep(7000);
                    await Task.Delay(TimeSpan.FromSeconds(5));

                    //{Thread.CurrentContext.ContextID} 
                    //{Thread.CurrentThread.IsThreadPoolThread} 
                    //{Thread.CurrentThread.Name} 
                    Console.WriteLine($"{Thread.CurrentThread.ManagedThreadId:D5} {i:D2}/{imax:D2} sec lock={message.LockedUntilUtc:T} now={DateTime.UtcNow:T} expires={message.ExpiresAtUtc} {message.MessageId} {message.LockToken}");
                }

                while (!cancellationToken.IsCancellationRequested)
                {
                    log.WriteLine("From function: Cancelled: No");
                    Thread.Sleep(2000);
                }

                await message.CompleteAsync();
            }
            catch (Exception ex)
            {
                if (message.DeliveryCount >= 10)
                {
                    await message.DeadLetterAsync(ex.Message, ex.StackTrace);
                    Logger.Error($"Command: {message.MessageId} failed", ex);


                    // так вручную отсылается Exception в AppInsights
                    // Set up some properties:
                    var properties = new Dictionary<string, string>{{"Game", currentGame.Name}};

                    var measurements = new Dictionary<string, double>{{"Users", currentGame.Users.Count}};

                    // Send the exception telemetry:
                    telemetry.TrackException(ex, properties, measurements); // TrackException is used to report exceptions because it sends a copy of the stack.

                    // еще есть
                    //telemetry.TrackEvent ("строка", properties, measurements);
                    // + к этому еще есть:
                    //telemetry.TrackTrace()
                }
                else
                {
                    await message.AbandonAsync();
                    Logger.Warn($"Command: {message.MessageId} abandoned", ex);
                }
                throw;
            }
        }


        //cron expression for time-triggered tasks
        //public static void FiveSecondTask([TimerTrigger("*/5 *  * * * *")] TimerInfo timer)
        //{
        //    Console.WriteLine("This should run every 5 seconds");
        //}


        public static void ErrorMonitor(
            [ErrorTrigger("0:30:00", 10, Throttle = "1:00:00")] TraceFilter filter,
            TextWriter log,
            [SendGrid(To = "admin@emailaddress.com",Subject = "Error!")] SendGridMessage message
            )
        {
            // send Text notification using IFTTT
            string body = string.Format("{{ \"value1\": \"{0}\" }}", filter.Message);
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, WebNotificationUri)
            {
                Content = new StringContent(body, Encoding.UTF8, "application/json")
            };
            new HttpClient.SendAsync(request).Result;

            // log last 5 detailed errors to the Dashboard
            log.WriteLine(filter.GetDetailedMessage(5));

            message.Text = filter.GetDetailedMessage(1);
        }

        /* отсылка телеметрии на низком так сказать уровне https://stackoverflow.com/questions/32469014/use-azure-application-insights-with-azure-webjob/32469626
        TelemetryConfiguration.Active.InstrumentationKey = "the_key";
        TelemetryConfiguration.Active.TelemetryChannel.DeveloperMode = true;

        var tc = new TelemetryClient();
        tc.TrackRequest("Track Some Request", DateTimeOffset.UtcNow, new TimeSpan(0, 0, 3), "200", true);
        tc.TrackMetric("XYZ Metric", 100);
        tc.TrackEvent("Tracked Event");

        tc.Flush(); //need to do this, otherwise if the app exits the telemetry data won't be sent
        */
    }
}
