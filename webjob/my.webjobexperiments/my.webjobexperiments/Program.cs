﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ApplicationInsights.Channel;
using Microsoft.Azure;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Azure.WebJobs.Logging;
using Microsoft.Azure.WebJobs.Logging.ApplicationInsights;
using Microsoft.Azure.WebJobs.ServiceBus;
using Microsoft.Extensions.Logging;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;


/* TODO: 

    Задачи: 
    - знать, сколько потоков одновременно запущено - раз
    - знать, на обработке какого именно сообщения мы зависли - обязательно
    - - по-возможности, удобно это мониторить
    - - по-возможности, записать это
    - статиска обработки сообщений - мы должны условно представлять, сколько времени занимает обработка того или иного сообщения
    - перехватывать все возможные и не возможные ошибки, в том числе и до запуска host.runandblock видимо. Unhandled exceptions и т.д.
    - обязательно трэкать correlationId - это свойство BrokeredMessage, message properties. ЗАПОЛНЕНО ОНО НЕ ВСЕГДА! А в принципе
      у всех сообщений оно должно быть заполнено и начинаться с API request!!!! и видимо им же и заканчиваться. Иначе как?
      Есть MessageId и CorrelationId
      log: EnqueuedTimeUtc, DeliveryCount




    - Вопрос: как вызвать Kudu? How to call webjob dashboard (and what is it)? 
    - Answer: add *.scm.* to url.

    - где трекается exception, который возникает при слишком длинном времени обработки
      -

    - что видно в application insights, что в kudu
    - логи - где какие и сколько их. есть dashboard, есть kudu, есть log4net, есть insights
    - что такое профайлинг? было такое меню где-то, вроде в kudu или в app service settings


    https://github.com/advancedrei/ApplicationInsights.Helpers - использовались в 3nm, давно не обновлялись. Посмотреть, что это
    Эти хелперы инициализируют TelemetryContext различной инфой, через реализацию интерфейса IContextInitializer, а именно:

    _sourceAssembly это обычно Assembly.GetCallingAssembly()
        public void Initialize(TelemetryContext context)
        {
            context.Component.Version = _sourceAssembly.GetName().Version.ToString();

            context.Device.Language = CultureInfo.CurrentUICulture.IetfLanguageTag;
            context.Device.OperatingSystem = Environment.OSVersion.ToString();

            context.Properties.Add("64BitOS", Environment.Is64BitOperatingSystem.ToString());
            context.Properties.Add("64BitProcess", Environment.Is64BitProcess.ToString());
            context.Properties.Add("MachineName", Environment.MachineName);
            context.Properties.Add("ProcessorCount", Environment.ProcessorCount.ToString());
            context.Properties.Add("ClrVersion", Environment.Version.ToString());

            context.Session.Id = DateTime.UtcNow.ToString();
            context.Session.IsFirst = true;

            //context.User.AccountId = Environment.UserDomainName;
            //context.User.Id = Environment.UserName;
        }

    TelemetryClient.Flush() - use to make sure you not lost any telemetry

    use cloud_RoleName property to indetify different components within the same telemetry channel

    Важно! Вот в чем отличие между Console и TextWriter в функции:
    Output from Console methods that you call in a function or in the Main() method appears in the Dashboard page for the WebJob, 
    not in the page for a particular method invocation. Output from the TextWriter object that you get from a parameter in your 
    method signature appears in the Dashboard page for a method invocation.
    Console output can't be linked to a particular method invocation because the Console is single-threaded, 
    while many job functions may be running at the same time. That's why the SDK provides each function invocation 
    with its own unique log writer object.

 */
namespace my.webjobexperiments
{
    // To learn more about Microsoft Azure WebJobs SDK, please see https://go.microsoft.com/fwlink/?LinkID=320976
    
    // or better - https://github.com/Azure/azure-webjobs-sdk/wiki
    class Program
    {
        private static string _servicesBusConnectionString;
        private static NamespaceManager _namespaceManager;
        private static CloudQueue _testQueue;

        
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main()
        {
            using (var loggerFactory = new LoggerFactory())
            {
                var config = new JobHostConfiguration();
                config.Tracing.ConsoleLevel = TraceLevel.Verbose;
                config.HostId = "myexperiments";

            // подумать, надо ли это
            config.Aggregator.BatchSize = 10;
            config.Aggregator.FlushTimeout = TimeSpan.FromSeconds(10);
            config.Aggregator.IsEnabled = true;

            //config.LoggerFactory = надо проинициализировать по идее.
            


            // If this variable exists, build up a LoggerFactory with ApplicationInsights and a Console Logger
            string instrumentationKey = Environment.GetEnvironmentVariable("APPINSIGHTS_INSTRUMENTATIONKEY");
            if (String.IsNullOrEmpty(instrumentationKey))
            {
                //ConfigurationManager.AppSetting["APPINSIGHTS_INSTRUMENTATIONKEY"];
                instrumentationKey = CloudConfigurationManager.GetSetting("APPINSIGHTS_INSTRUMENTATIONKEY");
            }

            if (!string.IsNullOrEmpty(instrumentationKey))
            {
                // Wire up with default filters; Filtering will be explained later.
                //install Microsoft.Extensions.Logging to get access to LoggerFactory
                config.LoggerFactory = loggerFactory.AddApplicationInsights(instrumentationKey, null)
                    .AddConsole();//  .AddDebug().AddAzureWebAppDiagnostics();

                /*LogCategoryFilter: и соотв. CategoryLevels: Trace, Devbug, Information (Default), Warning, Error, Critical
                var filter = new LogCategoryFilter();
                filter.DefaultLevel = LogLevel.Warning;
                filter.CategoryLevels[LogCategories.Function] = LogLevel.Error;
                filter.CategoryLevels[LogCategories.Results] = LogLevel.Error;
                filter.CategoryLevels["Host.Triggers"] = LogLevel.Debug;

                config.LoggerFactory = new LoggerFactory()
                    .AddApplicationInsights(instrumentationKey, filter.Filter)
                    .AddConsole(filter.Filter);
                */

                config.Tracing.ConsoleLevel = TraceLevel.Verbose;
            }
            // to get instance of Logger factory - use JobHostConfiguration.LoggerFactory.
            // внутри функции ILogger - это и будет сгенерированный loggerFactory логгер
            // еще можно ILogger<Program> logger = loggerFactory.CreateLogger<Program>();


            if (config.IsDevelopment)
            {
                config.UseDevelopmentSettings(); // for details see the following: https://github.com/Azure/azure-webjobs-sdk/wiki/Running-Locally 
                // In particualr, it does the following:
                // - sets JobHostConfiguration.Tracing.ConsoleLevel to TraceLevel.Verbose to maximize logging output
                // - sets JobHostConfiguration.Queues.MaxPollingInterval to a low value to ensure queue methods are triggered immediately
                // - sets JobHostConfiguration.Singleton.ListenerLockPeriod to 15 seconds to aid in rapid iterative development
            }

            //for different .Use* see: https://github.com/Azure/azure-webjobs-sdk-extensions

            //config.UseTimers();// Continuous and scheduled CRON webjobs require 'Always on' to be enabled for your app.
            config.UseCore(); // you need tthis to use ErrorTrigger

            _servicesBusConnectionString = AmbientConnectionStringProvider.Instance.GetConnectionString(ConnectionStringNames.ServiceBus);
            _namespaceManager = NamespaceManager.CreateFromConnectionString(_servicesBusConnectionString);

            config.UseServiceBus( new ServiceBusConfiguration {
                ConnectionString = _servicesBusConnectionString,
                    MessageOptions = new OnMessageOptions {
                        MaxConcurrentCalls = 16,
                        AutoRenewTimeout = TimeSpan.FromMinutes(10),// https://stackoverflow.com/questions/33391888/service-bus-message-abandoned-despite-webjobs-sdk-handler-completed-successfully/33403158#33403158
                                                                    // не понял, как это работает. Не увидел.
                                                                    // If the function runs longer than the PeekLock timeout, the lock is automatically renewed.

                        AutoComplete = false // you don't have to call message.CompleteAsync() at the end of processing?
                    }
                });
                //config.UseFiles();// to monitor file changes, see https://github.com/Azure/azure-webjobs-sdk-extensions

                //is it for queues or service bus topics?
                config.Queues.BatchSize = 2;// ПРОВЕРИТЬ - сколько у нас??? ведь один процесс, может обрабатывать параллельно несколько сообщений
                config.Queues.MaxDequeueCount = 3;//number of retries before moving to poison queue, default value=5
                config.Queues.MaxPollingInterval = TimeSpan.FromSeconds(5);// maximum interval. By default - it uses flexible algorithm, which increase time up to 10 minutes

            /*
            config.UseSendGrid(new SendGridConfiguration()
            {
                FromAddress = new MailAddress("orders@webjobssamples.com", "Order Processor")
            });
            */

                var host = new JobHost(config);
                // The following code ensures that the WebJob will be running continuously
                host.RunAndBlock();
            }// to ensure all logs will be flushed
        }

        /* custom factory:
        private class CustomTelemetryClientFactory : DefaultTelemetryClientFactory
        {
            public CustomTelemetryClientFactory(string instrumentationKey, Func<string, LogLevel, bool> filter)
                : base(instrumentationKey, new SamplingPercentageEstimatorSettings(), filter)
            {
            }

            protected override ITelemetryChannel CreateTelemetryChannel()
            {
                ServerTelemetryChannel channel = new ServerTelemetryChannel();

                // change the default from 30 seconds to 15 seconds
                channel.MaxTelemetryBufferDelay = TimeSpan.FromSeconds(15);

                return channel;
            }
        }
        var clientFactory = new CustomTelemetryClientFactory(instrumentationKey, filter.Filter);

        config.LoggerFactory = new LoggerFactory().AddApplicationInsights(clientFactory);
        */



        private static void CreateTestQueues()
        {
            string connectionString = AmbientConnectionStringProvider.Instance.GetConnectionString(ConnectionStringNames.Storage);
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();

            CloudQueue queue = queueClient.GetQueueReference("singleton-test");
            queue.CreateIfNotExists();

            _testQueue = queueClient.GetQueueReference("testqueue");
            _testQueue.CreateIfNotExists();

            CloudQueue testQueue2 = queueClient.GetQueueReference("testqueue2");
            testQueue2.CreateIfNotExists();
        }
    }
}
